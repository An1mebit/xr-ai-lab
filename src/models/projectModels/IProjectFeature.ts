import { LocaleTypes } from "../../locale/locale";

export interface IProjectFeature {
  id?: string;
  titleImage?: string;
  businessTaskImages?: string[];
  projectData: {
    [key in LocaleTypes]: {
      title?: string;
      subTitle?: string;
      description?: string;
      data?: {
        date?: string;
        year?: string;
        customer?: string;
        command?: {
          role?: string;
          FIO?: string;
        }[];
      };
      businessTask?: string;
      explanations?: {
        title?: string;
        description?: string;
      }[];
      links?: {
        name?: string;
        href?: string;
      }[];
    };
  };
}
