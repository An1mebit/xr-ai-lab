import { LocaleTypes } from "../../locale/locale";

export enum ProjectTypes {
  link = "link",
  current = "current",
}

export interface IProject {
  id?: string;
  projectData: {
    [key in LocaleTypes]: {
      title?: string;
      subTitle?: string;
      description?: string;
    };
  };
  type: ProjectTypes;
  linkToProject?: string;
  imagePath?: string;
}
