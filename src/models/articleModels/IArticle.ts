import { ArticleType } from "../../components/ArticleFabric/ArticleFabric";
import { MediaType } from "../../components/MediaLink/MediaLink";
import { LocaleTypes } from "../../locale/locale";

export interface IArticle {
  filePath: string;
  articleData: {
    [key in LocaleTypes]: {
      heading: string;
      authors: string[];
      journal: string;
    };
  };

  year: string;
  isVidio: boolean;
  mediaPath: string;
  imagePath?: string;
  mediaType: MediaType;
  size?: ArticleType;
}
