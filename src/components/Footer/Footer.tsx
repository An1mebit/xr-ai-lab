import styles from "./footer.module.scss";
import { ReactComponent as Arrow } from "../../assets/arrow-up.svg";
import { FormattedMessage } from "react-intl";
import { ReactComponent as Logo } from "../../assets/logo.svg";
import { useLocation } from "react-router-dom";
import useWindowSize from "../../hooks/useWindowSize";
import { mobileWidth } from "../../config/config";
import { CSSProperties } from "react";
import useScroll from "../../hooks/useScroll";

export const Footer = () => {
  const handlerScrollToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  };

  const location = useLocation();
  const windowSize = useWindowSize();
  const scrollHeight = useScroll();

  const getMargin = (): CSSProperties => {
    if (location.pathname === "/contacts" && windowSize < mobileWidth - 159) {
      return {
        marginTop: "-239px",
      };
    } else {
      return {};
    }
  };

  const getArrowDisplay = (): CSSProperties => {
    if (windowSize >= mobileWidth - 200) {
      return {
        opacity: "1",
      };
    }
    if (
      windowSize < mobileWidth - 200 &&
      scrollHeight > window.innerHeight / 2
    ) {
      return {
        opacity: "1",
      };
    } else {
      return {
        opacity: "0",
      };
    }
  };

  return (
    <>
      <div className={styles.footer} style={getMargin()}>
        <p className={styles.logo}>
          <Logo />
        </p>
        <div className={styles.cross__links}>
          <a
            href="https://mai.ru/?referer=https%3A%2F%2Fyandex.ru%2F"
            target="_blank"
          >
            <FormattedMessage id={"footerMAI"} />
          </a>
          <a target="_blank">
            <FormattedMessage id={"footerSocial"} />
          </a>
          <a target="_blank">
            <FormattedMessage id={"footerTelegram"} />
          </a>
        </div>
        <div className={styles.arrow__button_up} style={getArrowDisplay()}>
          <button className={styles.button__up} onClick={handlerScrollToTop}>
            <Arrow className={styles.arrow__button} />
          </button>
        </div>
      </div>
    </>
  );
};
