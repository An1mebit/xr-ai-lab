import { FC } from "react";
import { IProject } from "../../models/projectModels/IProject";
import { LocaleTypes } from "../../locale/locale";
import { useIntl } from "react-intl";
import styles from "./projectCard.module.scss";
import { useNavigate } from "react-router-dom";

interface IProjectCardProps {
  project: IProject;
}

const ProjectCard: FC<IProjectCardProps> = ({ project }) => {
  const intl = useIntl();
  const navigate = useNavigate();

  const handlerDetailsClick = () => {
    navigate(`/projects/project/${project?.id || ""}`);
  };
  return (
    <div className={styles.project__card} onClick={handlerDetailsClick}>
      <div className={styles.project__gradient}></div>
      <div className={styles.project__illustration}>
        <img src={project.imagePath} alt="project illustration" />
      </div>
      <div className={styles.project__card_content}>
        <h4> {project?.projectData[intl.locale as LocaleTypes].title || ""}</h4>
        <p>
          {" "}
          {project?.projectData[intl.locale as LocaleTypes].subTitle || ""}
        </p>
      </div>
    </div>
  );
};

export default ProjectCard;
