import { FC } from "react";
import { IArticle } from "../../models/articleModels/IArticle";
import { MediaLink, MediaType } from "../MediaLink/MediaLink";
import styles from "./articleFabric.module.scss";
import { FormattedMessage, useIntl } from "react-intl";
import { LocaleTypes } from "../../locale/locale";

export enum ArticleType {
  large = "l",
  medium = "m",
  small = "s",
}

interface IArticleFabricProps {
  article: IArticle;
  size?: ArticleType;
  mediaType: MediaType;
}

export const ArticleFabric: FC<IArticleFabricProps> = ({
  article,
  size,
  mediaType,
}) => {
  const intl = useIntl();

  const authorsConverter = (authors: string[]): string => {
    let authorsInline = "";
    authors.forEach((author, index) => {
      if (author.length !== index - 1) {
        authorsInline += author + ", ";
      } else {
        authorsInline += author;
      }
    });
    return authorsInline;
  };

  switch (size) {
    case ArticleType.large:
      return (
        <div className={styles.article__large}>
          <img src={article?.imagePath} height={"auto"} />
          <div className={styles.article__body}>
            <div className={styles.article__heading}>
              <MediaLink mediaPath={article.mediaPath} mediaType={mediaType} />
              <MediaLink
                mediaPath={article.filePath}
                mediaType={MediaType.pdf}
              />
            </div>
            <div className={styles.article__text}>
              <h4>
                {article?.articleData[intl.locale as LocaleTypes].heading ??
                  "Заголовок"}
              </h4>
              <p>
                <FormattedMessage id={"articleAuthors"} />:{" "}
                {authorsConverter(
                  article?.articleData[intl.locale as LocaleTypes].authors ?? []
                )}
              </p>
              <div className={styles.article__journal}>
                <p>
                  <FormattedMessage id={"articleJournals"} />:{" "}
                  {article?.articleData[intl.locale as LocaleTypes].journal ??
                    "Журнал"}
                </p>
                <p>
                  {article?.year} <FormattedMessage id={"articleYear"} />
                </p>
              </div>
            </div>
          </div>
        </div>
      );
    case ArticleType.medium:
      return (
        <div className={styles.article__large}>
          <div className={styles.article__body}>
            <div className={styles.article__heading}>
              <MediaLink mediaPath={article.mediaPath} mediaType={mediaType} />
              <MediaLink
                mediaPath={article.filePath}
                mediaType={MediaType.pdf}
              />
            </div>
            <div className={styles.article__text}>
              <h4>
                {article?.articleData[intl.locale as LocaleTypes].heading ??
                  "Заголовок"}
              </h4>
              <p>
                {" "}
                <FormattedMessage id={"articleAuthors"} />:{" "}
                {authorsConverter(
                  article?.articleData[intl.locale as LocaleTypes].authors ?? []
                )}
              </p>
              <div className={styles.article__journal}>
                <p>
                  <FormattedMessage id={"articleJournals"} />:{" "}
                  {article?.articleData[intl.locale as LocaleTypes].journal ??
                    "Журнал"}
                </p>
                <p>
                  {article?.year} <FormattedMessage id={"articleYear"} />
                </p>
              </div>
            </div>
          </div>
        </div>
      );
    case ArticleType.small:
      return (
        <div className={styles.article__small}>
          <div className={styles.article__body}>
            <div className={styles.article__heading}>
              <MediaLink mediaPath={article.mediaPath} mediaType={mediaType} />
              <MediaLink
                mediaPath={article.filePath}
                mediaType={MediaType.pdf}
              />
            </div>
            <div className={styles.article__text}>
              <h4>
                {article?.articleData[intl.locale as LocaleTypes].heading ??
                  "Заголовок"}
              </h4>
              <p>
                {" "}
                <FormattedMessage id={"articleAuthors"} />:{" "}
                {authorsConverter(
                  article?.articleData[intl.locale as LocaleTypes].authors ?? []
                )}
              </p>
              <div className={styles.article__journal}>
                <p>
                  <FormattedMessage id={"articleJournals"} />:{" "}
                  {article?.articleData[intl.locale as LocaleTypes].journal ??
                    "Журнал"}
                </p>
                <p>
                  {article?.year} <FormattedMessage id={"articleYear"} />
                </p>
              </div>
            </div>
          </div>
        </div>
      );
    default:
      break;
  }
  return <></>;
};
