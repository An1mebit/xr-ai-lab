import { FC } from "react";
import Arrow from "../../assets/arrow-up.svg";
import styles from "./mediaLink.module.scss";
import { FormattedMessage } from "react-intl";

export enum MediaType {
  article = "Статья",
  pdf = "PDF",
  video = "Видео",
  Custom = "",
}

interface IMediaLinkProps {
  mediaPath: string;
  mediaType: MediaType;
  mediaName?: string;
}

export const MediaLink: FC<IMediaLinkProps> = ({
  mediaPath,
  mediaType,
  mediaName,
}) => {
  switch (mediaType) {
    case MediaType.article:
      return (
        <div className={styles.media__link}>
          <a href={mediaPath} target="_blank">
            <FormattedMessage id={"articleLink"} />
          </a>
          <img src={Arrow} alt="arrow" />
        </div>
      );
    case MediaType.pdf:
      return (
        <div className={styles.media__link}>
          <a href={mediaPath} target="_blank">
            {MediaType.pdf}
          </a>{" "}
          <img src={Arrow} alt="arrow" />
        </div>
      );
    case MediaType.video:
      return (
        <div className={styles.media__link}>
          <a href={mediaPath} target="_blank">
            <FormattedMessage id={"articleVideo"} />
          </a>{" "}
          <img src={Arrow} alt="arrow" />
        </div>
      );
    case MediaType.Custom:
      return (
        <div className={styles.media__link}>
          <a href={mediaPath} target="_blank">
            {mediaName}
          </a>{" "}
          <img src={Arrow} alt="arrow" />
        </div>
      );
  }
};
