import { Link, useLocation } from "react-router-dom";
import styles from "./header.module.scss";
import { headerLinksMap } from "../Constants/headerConstants";
import { Dispatch, SetStateAction, FC, CSSProperties } from "react";
import { mobileWidth } from "../../config/config";
import { HamburgerMenu } from "../HamburgerMenu/HamburgerMenu";
import { LocaleTypes } from "../../locale/locale";
import { FormattedMessage, useIntl } from "react-intl";
import useWindowSize from "../../hooks/useWindowSize";
import { ReactComponent as Logo } from "../../assets/logo.svg";
import useScroll from "../../hooks/useScroll";

interface IHeaderProps {
  setLanguage: Dispatch<SetStateAction<string>>;
}

export const Header: FC<IHeaderProps> = ({ setLanguage }) => {
  const location = useLocation();
  const windowSize = useWindowSize();
  const scrollHeight = useScroll();
  const intl = useIntl();

  const handlerLinkClick = () => {
    window.scrollTo(0, 0);
  };

  const handlerChangeLocalization = (value: LocaleTypes) => {
    setLanguage(value);
  };

  const handlerChangeOpacity = (location: LocaleTypes): boolean => {
    return location === intl.locale ? true : false;
  };

  const handlerChangeHeaderOpacity = (): CSSProperties => {
    if (
      scrollHeight < window.innerHeight &&
      (location.pathname === "/" || location.pathname === "/contacts")
    ) {
      return {
        background: "rgba(34, 34, 41, 0)",
        backdropFilter: "none",
      };
    } else {
      return {
        background: "rgba(34, 34, 41, 0.8)",
        backdropFilter: "blur(16px)",
      };
    }
  };

  return (
    <>
      <div className={styles.header} style={handlerChangeHeaderOpacity()}>
        <Link to={"/"} className={styles.logo} onClick={handlerLinkClick}>
          <Logo />
        </Link>
        {windowSize >= mobileWidth ? (
          <div className={styles.navlinks__container}>
            {headerLinksMap.map((item, index) => (
              <div
                key={index}
                className={
                  location.pathname === item.link
                    ? `${styles.navlink__box} ${styles.navlink}`
                    : `${styles.navlink__box}`
                }
                style={{ verticalAlign: "middle", alignItems: "center" }}
              >
                <Link
                  to={item.link}
                  onClick={handlerLinkClick}
                  style={{ lineHeight: "75px" }}
                >
                  <FormattedMessage id={item.id}></FormattedMessage>
                </Link>
              </div>
            ))}
          </div>
        ) : (
          <></>
        )}
        <div
          className={styles.language}
          style={windowSize >= mobileWidth ? {} : { marginRight: "10px" }}
        >
          <button
            onClick={() => handlerChangeLocalization(LocaleTypes.ru)}
            style={{
              opacity: handlerChangeOpacity(LocaleTypes.ru) ? "1" : "0.5",
            }}
          >
            RU
          </button>
          <button
            onClick={() => handlerChangeLocalization(LocaleTypes.en)}
            style={{
              opacity: handlerChangeOpacity(LocaleTypes.en) ? "1" : "0.5",
            }}
          >
            EN
          </button>
          {windowSize >= mobileWidth ? <></> : <HamburgerMenu />}
        </div>
      </div>
    </>
  );
};
