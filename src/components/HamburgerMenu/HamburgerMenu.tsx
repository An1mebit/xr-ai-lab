import { Link } from "react-router-dom";
import { headerLinksMap } from "../Constants/headerConstants";
import "./hamburgerMenu.scss";

export const HamburgerMenu = () => {
  return (
    <div className="hamburger__container">
      <input type="checkbox" id="active" />
      <label htmlFor="active" className="menu__btn">
        <span></span>
      </label>
      <label htmlFor="active" className="close"></label>
      <div className="menu__container">
        {headerLinksMap.map((item, index) => (
          <Link
            key={index}
            to={item.link}
            className={location.pathname === item.link ? "navlink" : ""}
          >
            {item.name}
          </Link>
        ))}
      </div>
    </div>
  );
};
