import { useState } from "react";
import styles from "./contacts.module.scss";
import { ContactsInfo } from "../../ContactsInfo/ContactsInfo";
import { FormattedMessage } from "react-intl";

export const Contacts = () => {
  const [email, setEmail] = useState<string>("");
  const [isVerify, setIsVerify] = useState<boolean>(false);

  const handlerSendEmail = () => {
    //request for offers
    console.log(email + " " + isVerify);
  };

  const handlerChangeVerify = () => {
    setIsVerify(!isVerify);
  };

  return (
    <div className={`container ${styles.contacts__wrapper}`}>
      <div className={styles.contacts__item}>
        <h2>
          <FormattedMessage id={"contactsTitle"} />
        </h2>
        <div className={styles.location__wrapper}>
          <ContactsInfo />
        </div>
      </div>
      <div
        className={styles.contacts__item + " " + styles.contacts__contact_info}
      >
        <h3>
          <FormattedMessage id={"contactsCooperationTitle"} />
          <span style={{ whiteSpace: "nowrap" }}>
            <FormattedMessage id={"contactsCooperationTitleSpan"} />
          </span>
        </h3>
        <p>
          <FormattedMessage id={"contactsCooperationDescription"} />
        </p>
        <input
          type="email"
          placeholder="email"
          value={email ?? ""}
          onChange={(e) => setEmail(e.target.value)}
          className={styles.email__input}
        />
        <div className={styles.confirm__personal_data}>
          <input
            type="checkbox"
            checked={isVerify}
            onChange={handlerChangeVerify}
            className={styles.checkbox__input}
            id="check1"
          />
          <label htmlFor="check1">
            <span></span>
          </label>
          <p>
            <FormattedMessage id={"contactsCooperationProcessing"} />
            <span>
              <FormattedMessage id={"contactsCooperationSpan"} />
            </span>
          </p>
        </div>
        <button
          onChange={handlerSendEmail}
          disabled={!isVerify || email.length === 0}
        >
          <FormattedMessage id={"contactsSend"} />
        </button>
      </div>
    </div>
  );
};
