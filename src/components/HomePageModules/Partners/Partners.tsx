import styles from "./partners.module.scss";
import Uac from "../../../assets/uac-logo.svg";
import Svo from "../../../assets/svo-logo.svg";
import Yandex from "../../../assets/yandex-logo.svg";
import Ivi from "../../../assets/ivi-logo.svg";
import Ock from "../../../assets/ock-logo.svg";
import Sber from "../../../assets/sber-logo.svg";
import { FormattedMessage } from "react-intl";
import useWindowSize from "../../../hooks/useWindowSize";
import { CarouselProvider, Slide, Slider } from "pure-react-carousel";
import "pure-react-carousel/dist/react-carousel.es.css";

const images: string[] = [Uac, Svo, Yandex, Ivi, Sber, Ock];

export const Partners = () => {
  const windowSize = useWindowSize();
  return (
    <div className={`container_inner_item ${styles.container__wrapper}`}>
      <h2>
        <FormattedMessage id={"partnersTitle"} />
      </h2>
      {windowSize >= 650 ? (
        <div className={styles.partners__wrapper}>
          {images.map((item, index) => (
            <div key={index} className={styles.partners__item}>
              <img src={item} />
            </div>
          ))}
        </div>
      ) : (
        <div className="carousel-container">
          <CarouselProvider
            naturalSlideWidth={100}
            naturalSlideHeight={125}
            totalSlides={images.length}
            infinite={true}
            visibleSlides={1.1}
            playDirection="forward"
            isPlaying
            className={styles.partners__carousel}
          >
            <Slider style={{ height: "50vh" }}>
              {images.map((item, index) => (
                <Slide key={index} index={index}>
                  <div className={styles.partners__item}>
                    <img src={item} />
                  </div>
                </Slide>
              ))}
            </Slider>
          </CarouselProvider>
        </div>
      )}
    </div>
  );
};
