import { FormattedMessage } from "react-intl";
import styles from "./AboutLab.module.scss";

export const AboutLab = () => {
  return (
    <div className="container">
      <div className={styles.container__about_first}>
        <div className={styles.container__about_item}>
          {" "}
          <h2>
            <FormattedMessage id={"aboutTitle"} />
          </h2>
        </div>
        <div className={styles.container__about_item}>
          {" "}
          <h6>
            <FormattedMessage id={"aboutTitleDescription"} />
          </h6>
        </div>
      </div>
      <div className={styles.container__about_second}>
        <div className={styles.container__about_item}>
          <h4>
            <FormattedMessage id={"aboutMainTitle"} />
          </h4>
          <h6>
            <FormattedMessage id={"aboutMainDescription"} />
          </h6>
        </div>
        <div className={styles.container__about_item}>
          <h4>
            <FormattedMessage id={"aboutAreaTitle"} />
          </h4>
          <h6>
            <FormattedMessage id={"aboutAreaDescription"} />
          </h6>
        </div>
      </div>
      <div className={styles.container__about_third}>
        <div className={styles.container__about_item_pref}>
          <h3>11</h3>
          <p>
            <FormattedMessage id={"aboutProjectsCount"} />
          </p>
        </div>
        <div className={styles.container__about_item_pref}>
          <h3>25 +</h3>
          <p>
            <FormattedMessage id={"aboutEmployeeCount"} />
          </p>
        </div>
        <div className={styles.container__about_item_pref}>
          <h3>400 +</h3>
          <p>
            <FormattedMessage id={"aboutStudentsCount"} />
          </p>
        </div>
      </div>
    </div>
  );
};
