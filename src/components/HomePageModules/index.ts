import { AboutLab } from "./AboutLab/AboutLab";
import { Contacts } from "./Contacts/Contacts";
import { Partners } from "./Partners/Partners";
import { ProjectsCarousel } from "./ProjectsCarousel/ProjectsCarousel";
import { SearchProject } from "./SearchProject/SearchProject";

export { AboutLab, Contacts, Partners, ProjectsCarousel, SearchProject };
