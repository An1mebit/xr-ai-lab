import styles from "./projectsCarousel.module.scss";
import {
  CarouselProvider,
  Slider,
  Slide,
  ButtonBack,
  ButtonNext,
} from "pure-react-carousel";
import "pure-react-carousel/dist/react-carousel.es.css";
import { FormattedMessage } from "react-intl";
import { useNavigate } from "react-router";
import useWindowSize from "../../../hooks/useWindowSize";

export const ProjectsCarousel = () => {
  const navigate = useNavigate();
  const windowSize = useWindowSize();

  return (
    <div className="container__white">
      <div className={styles.projects__container_wrapper}>
        <h2>
          <FormattedMessage id={"projectsTitle"} />
        </h2>
        <div className="carousel-container">
          <CarouselProvider
            naturalSlideWidth={100}
            naturalSlideHeight={125}
            totalSlides={2}
            infinite={true}
            visibleSlides={windowSize > 1100 ? 1 + windowSize / 5000 : 1}
            className={styles.projects__carousel}
          >
            <Slider style={{ height: "50vh" }}>
              <Slide index={0}>
                <div className={styles.projects__carousel_image_container}>
                  <img
                    src="https://avatars.mds.yandex.net/i?id=4668b3ce58192b841cf0486cc34d346f5ee25449-7765754-images-thumbs&n=13"
                    alt="cat"
                    className={styles.carousel__img}
                  />
                </div>
                <p>Моделирование рельефа реки</p>
              </Slide>
              <Slide index={1} className={styles.carousel__img}>
                <div className={styles.projects__carousel_image_container}>
                  <img
                    src="https://lookw.ru/8/828/1476173423-125.jpg"
                    alt="cat"
                    className={styles.carousel__img}
                  />
                </div>
                <p>Просто кот </p>
              </Slide>
            </Slider>
            <div className={styles.button_group__container}>
              <ButtonBack
                className={`${styles.partners__carousel_prev} ${styles.carousel__white_button}`}
              >
                <i className={`${styles.arrow} ${styles.left}`}></i>
              </ButtonBack>
              <ButtonNext
                className={`${styles.partners__carousel_next} ${styles.carousel__white_button}`}
              >
                <i className={`${styles.arrow} ${styles.right}`}></i>
              </ButtonNext>
            </div>
          </CarouselProvider>
        </div>

        <button onClick={() => navigate("/projects")}>
          <FormattedMessage id={"seeAllButton"} />
        </button>
      </div>
    </div>
  );
};
