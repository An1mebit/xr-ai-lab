import styles from "./searchProject.module.scss";
import Video from "../../../assets/mai.mp4";
import { FC } from "react";
import { IProject } from "../../../models/projectModels/IProject";
import { FormattedMessage, useIntl } from "react-intl";
import { LocaleTypes } from "../../../locale/locale";
import { useNavigate } from "react-router-dom";

type searchTypes = "home" | "project";

interface ISearchProjectProps {
  type: searchTypes;
  project?: IProject;
}

export const SearchProject: FC<ISearchProjectProps> = ({ type, project }) => {
  const intl = useIntl();
  const navigate = useNavigate();

  const handlerNavigateToProject = () => {
    navigate("/projects/project/2");
  };

  switch (type) {
    case "home": {
      return (
        <div className={styles.container__search} style={{ height: "auto" }}>
          <div className={styles.search__gradient}></div>
          <div className={styles.search__project}>
            <h1>XR-AI LAB</h1>
            <h4>
              <FormattedMessage id="searchSubTitle" />
            </h4>
            <button onClick={handlerNavigateToProject}>
              <FormattedMessage id="searchProject" /> <span className="arrow" />
            </button>
          </div>
          <div className={styles.container__illustration}>
            <video autoPlay loop muted playsInline>
              <source src={Video} type="video/mp4" />
            </video>
          </div>
        </div>
      );
    }
    case "project":
      return (
        <div className={styles.container__search} style={{ height: "auto" }}>
          <div className={styles.search__gradient}></div>
          <div
            className={styles.search__project}
            style={{ paddingBottom: "0px" }}
          >
            <h1>
              {project?.projectData[intl.locale as LocaleTypes].title ||
                "Название проекта"}
            </h1>
            <div
              style={{
                wordWrap: "break-word",
                maxWidth: "600px",
                minWidth: "300px",
              }}
            >
              <h4>
                {project?.projectData[intl.locale as LocaleTypes].subTitle ||
                  "Что-то о проекте"}
              </h4>
            </div>
            <form action={project?.linkToProject || ""} target="blank">
              <button>
                <FormattedMessage id={"seeButton"} />
                <span className="arrow" />
              </button>
            </form>
          </div>
          <div className={styles.container__illustration}>
            <img src={project?.imagePath || ""} alt="project illustration" />
          </div>
        </div>
      );
  }
};
