import { FormattedMessage } from "react-intl";
import styles from "../HomePageModules/Contacts/contacts.module.scss";

export const ContactsInfo = () => {
  return (
    <>
      <h5>
        <FormattedMessage id={"contactsLocation"} values={{ br: <br /> }} />
      </h5>
      <div className={styles.contacts__info_wrapper}>
        <div>
          <h5>+7 (915) 234-17-26</h5>
          <p>
            <FormattedMessage id={"contactsWhereCall"} />
          </p>
        </div>
        <div>
          <h5>itcentrmai@gmail.com</h5>
          <p>
            <FormattedMessage id={"contactsHow"} />
          </p>
        </div>
      </div>
    </>
  );
};
