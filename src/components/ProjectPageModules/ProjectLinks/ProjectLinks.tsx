import { FC } from "react";
import { FormattedMessage } from "react-intl";
import styles from "./projectLinks.module.scss";
import { MediaLink, MediaType } from "../../MediaLink/MediaLink";

interface IProjectLinksProps {
  links?: {
    name?: string;
    href?: string;
  }[];
}

export const ProjectLinks: FC<IProjectLinksProps> = ({ links }) => {
  if (links === undefined) {
    return <></>;
  }

  return (
    <div className="container_inner_item">
      <h2>
        <FormattedMessage id={"projectLinksTitle"} />
      </h2>
      <div className={styles.project__links_wrapper}>
        {links &&
          links.map((item, index) => (
            <MediaLink
              key={index}
              mediaType={MediaType.Custom}
              mediaPath={item?.href || ""}
              mediaName={item?.name || ""}
            />
          ))}
      </div>
    </div>
  );
};
