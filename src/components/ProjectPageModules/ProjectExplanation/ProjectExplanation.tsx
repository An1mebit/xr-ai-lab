import { FC } from "react";
import { FormattedMessage } from "react-intl";
import styles from "./projectExplanation.module.scss";

interface IProjectExplanationProps {
  explanations?: {
    title?: string;
    description?: string;
  }[];
}

export const ProjectExplanation: FC<IProjectExplanationProps> = ({
  explanations,
}) => {
  if (explanations === undefined) {
    return <></>;
  }

  return (
    <div className="container__white">
      <div className={styles.project__explanation_wrapper}>
        <h2>
          <FormattedMessage id={"projectExplanationTitle"} />
        </h2>
        <div className={styles.project__explanation_container}>
          {explanations &&
            explanations.map((item, index) => (
              <div className={styles.project__explanation_item} key={index}>
                <h2>{index + 1}</h2>
                <h4>{item?.title || ""}</h4>
                <p>{item?.description || ""}</p>
              </div>
            ))}
        </div>
      </div>
    </div>
  );
};
