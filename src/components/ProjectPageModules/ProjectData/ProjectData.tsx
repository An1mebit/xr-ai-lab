import { FC } from "react";
import { FormattedMessage } from "react-intl";
import styles from "./projectData.module.scss";

interface IProjectDataProps {
  data?: {
    date?: string;
    year?: string;
    customer?: string;
    command?: {
      role?: string;
      FIO?: string;
    }[];
  };
}

export const ProjectData: FC<IProjectDataProps> = ({ data }) => {
  const getCommandString = (command: { role?: string; FIO?: string }[]) => {
    const roles: string[] = [];

    command.forEach((member) => {
      if (roles.indexOf(member?.role || "") !== -1) {
        return;
      } else {
        roles.push(member?.role || "");
      }
    });
    let membersList = "";
    roles.forEach((role, index) => {
      membersList +=
        role +
        ": " +
        command
          .filter((item) => item.role === role)
          .map((item) => item.FIO)
          .join(", \n");
      if (index !== roles.length - 1) {
        membersList += ", \n";
      }
    });
    return membersList;
  };

  if (data === undefined) {
    return <></>;
  }

  return (
    <div className="container_inner_item">
      <h2>
        <FormattedMessage id={"projectDataTitle"} />
      </h2>
      <div className={styles.project__data_wrapper}>
        <div className={styles.project__data_item}>
          <p>
            <FormattedMessage id={"projectFeatureDate"} />
          </p>
          <h5>{data?.date || ""}</h5>
        </div>
        <div className={styles.project__data_item}>
          <p>
            <FormattedMessage id={"projectFeatureYear"} />
          </p>
          <h5>{data?.year || ""}</h5>
        </div>
        <div className={styles.project__data_item}>
          <p>
            <FormattedMessage id={"projectFeatureCustomer"} />
          </p>
          <h5>{data?.customer || ""}</h5>
        </div>
        <div className={styles.project__data_item}>
          <p>
            <FormattedMessage id={"projectFeatureCommand"} />
          </p>
          <h5 style={{ whiteSpace: "pre-line" }}>
            {getCommandString(data?.command || [])}
          </h5>
        </div>
      </div>
    </div>
  );
};
