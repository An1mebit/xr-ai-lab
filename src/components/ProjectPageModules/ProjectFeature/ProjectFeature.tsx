import { FC } from "react";
import styles from "./projectFeature.module.scss";
import { FormattedMessage } from "react-intl";

interface IProjectFeatureProps {
  description?: string;
}

export const ProjectFeature: FC<IProjectFeatureProps> = ({ description }) => {
  if (description === undefined) {
    return <></>;
  }

  return (
    <div className="container_inner_item">
      <div className={styles.project__feature_wrapper}>
        <div className={styles.project__feature_item}>
          <h2>
            <FormattedMessage id={"projectFeatureTitle"} />
          </h2>
        </div>
        <div className={styles.project__feature_item}>
          <p>{description}</p>
        </div>
      </div>
    </div>
  );
};
