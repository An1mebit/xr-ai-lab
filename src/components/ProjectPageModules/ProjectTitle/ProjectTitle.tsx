import { FC } from "react";
import styles from "./projectTitle.module.scss";

interface ProjectTitleProps {
  title?: string;
  subTitle?: string;
  titleImage?: string;
}

export const ProjectTitle: FC<ProjectTitleProps> = ({
  subTitle,
  title,
  titleImage,
}) => {
  if (
    subTitle === undefined &&
    title === undefined &&
    titleImage === undefined
  ) {
    return <></>;
  }

  return (
    <div className={styles.project__title}>
      <div className={styles.project__gradient_title}></div>
      <div className={styles.project__title_text}>
        <h1>{title}</h1>
        <h4>{subTitle}</h4>
      </div>
      <div className={styles.project__illustration_title}>
        <img src={titleImage || ""} alt="project illustration" />
      </div>
    </div>
  );
};
