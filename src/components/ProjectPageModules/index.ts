import { ProjectBusinessTask } from "./ProjectBusinessTask/ProjectBusinessTask";
import { ProjectData } from "./ProjectData/ProjectData";
import { ProjectExplanation } from "./ProjectExplanation/ProjectExplanation";
import { ProjectFeature } from "./ProjectFeature/ProjectFeature";
import { ProjectLinks } from "./ProjectLinks/ProjectLinks";
import { ProjectTitle } from "./ProjectTitle/ProjectTitle";

export {
  ProjectBusinessTask,
  ProjectData,
  ProjectExplanation,
  ProjectFeature,
  ProjectLinks,
  ProjectTitle,
};
