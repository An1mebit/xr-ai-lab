import { FC } from "react";
import { FormattedMessage } from "react-intl";
import {
  ButtonBack,
  ButtonNext,
  CarouselProvider,
  Slide,
  Slider,
} from "pure-react-carousel";
import "pure-react-carousel/dist/react-carousel.es.css";
import styles from "./projectBusinessTask.module.scss";
import useWindowSize from "../../../hooks/useWindowSize";

interface IProjectBusinessTaskProps {
  businessTask?: string;
  businessTaskImages?: string[];
}

export const ProjectBusinessTask: FC<IProjectBusinessTaskProps> = ({
  businessTask,
  businessTaskImages,
}) => {
  const windowSize = useWindowSize();

  if (businessTask === undefined && businessTaskImages === undefined) {
    return <></>;
  }

  return (
    <div className={"container_inner_item"}>
      <h2>
        <FormattedMessage id={"projectBusinessTaskTitle"} />
      </h2>
      <div className={styles.project__business_task_text}>
        <p>{businessTask}</p>
      </div>
      {businessTaskImages?.length === 0 ? (
        <></>
      ) : (
        <div className="carousel-container" style={{ marginTop: "160px" }}>
          <CarouselProvider
            naturalSlideWidth={100}
            naturalSlideHeight={125}
            totalSlides={businessTaskImages ? businessTaskImages.length : 1}
            infinite={true}
            visibleSlides={windowSize > 1100 ? 1 + windowSize / 5000 : 1}
            className={styles.project__carousel}
          >
            <Slider style={{ height: "50vh" }}>
              {businessTaskImages &&
                businessTaskImages.map((item, index) => (
                  <Slide index={index} key={index}>
                    <div className={styles.project__business_task_illustration}>
                      <img src={item} alt="image item" />
                    </div>
                  </Slide>
                ))}
            </Slider>
            <div className={styles.button_group__container}>
              <ButtonBack className={styles.partners__carousel_prev}>
                <i className={`${styles.arrow} ${styles.left}`}></i>
              </ButtonBack>
              <ButtonNext className={styles.partners__carousel_next}>
                <i className={`${styles.arrow} ${styles.right}`}></i>
              </ButtonNext>
            </div>
          </CarouselProvider>
        </div>
      )}
    </div>
  );
};
