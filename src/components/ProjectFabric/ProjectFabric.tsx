import { FC } from "react";
import { IProject, ProjectTypes } from "../../models/projectModels/IProject";
import { SearchProject } from "../HomePageModules/SearchProject/SearchProject";
import styles from "./projectFabric.module.scss";
import { useNavigate } from "react-router-dom";
import { FormattedMessage, useIntl } from "react-intl";
import { LocaleTypes } from "../../locale/locale";

interface IProjectFabricProps {
  project: IProject;
}

export const ProjectFabric: FC<IProjectFabricProps> = ({ project }) => {
  const navigate = useNavigate();

  const handlerDetailsClick = () => {
    navigate(`/projects/project/${project?.id || ""}`);
  };

  const intl = useIntl();
  switch (project.type) {
    case ProjectTypes.link: {
      return (
        <div>
          <SearchProject type="project" project={project} />
          <div
            className="container_inner_item"
            style={{ marginBottom: "160px" }}
          >
            <div className={styles.project__container_description}>
              <h2>
                {project?.projectData[intl.locale as LocaleTypes].title || ""}
              </h2>
              <div className={styles.project__text}>
                <p>
                  {project?.projectData[intl.locale as LocaleTypes]
                    .description || ""}
                </p>
              </div>
            </div>
          </div>
        </div>
      );
    }
    case ProjectTypes.current: {
      return (
        <div>
          <div className={styles.project__container_illustration}>
            <img src={project?.imagePath || ""} alt="project illustration" />
          </div>
          <div
            className="container_inner_item"
            style={{ marginBottom: "160px" }}
          >
            <div className={styles.project__container_description}>
              <h2>
                {project?.projectData[intl.locale as LocaleTypes].title || ""}
              </h2>
              <div className={styles.project__text}>
                <p>
                  {project?.projectData[intl.locale as LocaleTypes]
                    .description || ""}
                </p>
              </div>
            </div>
            <button
              className={styles.project__button}
              onClick={handlerDetailsClick}
            >
              <FormattedMessage id={"detailButton"} />{" "}
              <span className="arrow" />
            </button>
          </div>
        </div>
      );
    }
  }
};
