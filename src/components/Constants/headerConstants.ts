export type navlink = {
  id: string;
  link: string;
  name: string;
};

export const headerLinksMap: navlink[] = [
  {
    id: "headerProject",
    link: "/projects",
    name: "Проекты",
  },
  {
    id: "headerPublic",
    link: "/public",
    name: "Публикации",
  },
  {
    id: "headerArticle",
    link: "/articles",
    name: "Научные статьи",
  },
  {
    id: "headerContacts",
    link: "/contacts",
    name: "Контакты",
  },
];
