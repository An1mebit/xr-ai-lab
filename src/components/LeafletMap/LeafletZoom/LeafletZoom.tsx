import { useMap } from "react-leaflet";
import { FC } from "react";

interface ILeafletZoomProps {
  zoom: number;
}

const LeafletZoom: FC<ILeafletZoomProps> = ({ zoom }) => {
  const map = useMap();

  map.setZoom(zoom);

  return null;
};

export default LeafletZoom;
