import { MapContainer, TileLayer, Marker } from "react-leaflet";
import MarkerIcon from "../../assets/marker.svg";
import { Icon } from "leaflet";
import styles from "./leafletMap.module.scss";
import { FC } from "react";
import LeafletZoom from "./LeafletZoom/LeafletZoom";

interface ILeafletMapProps {
  zoom: number;
}

const icon = new Icon({
  iconUrl: MarkerIcon,
  iconSize: [90, 90],
  iconAnchor: [15, 28],
  className: styles.mapleaflet__icon,
});

const LeafletMap: FC<ILeafletMapProps> = ({ zoom }) => {
  return (
    <div className={styles.mapleaflet}>
      <div className={styles.mapleaflet__container}>
        <MapContainer
          zoom={zoom}
          center={{ lat: 55.810387, lng: 37.502089 }}
          dragging={false}
          keyboard={false}
          scrollWheelZoom={false}
        >
          <TileLayer
            attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          />
          <Marker position={{ lat: 55.811508, lng: 37.4973 }} icon={icon} />
          <LeafletZoom zoom={zoom} />
        </MapContainer>
      </div>
    </div>
  );
};

export default LeafletMap;
