import { Dispatch, SetStateAction, FC } from "react";
import styles from "./leafletZoom.module.scss";

interface ILeafletZoomControlProps {
  zoom: number;
  setZoom: Dispatch<SetStateAction<number>>;
}

const LeafletZoomControl: FC<ILeafletZoomControlProps> = ({
  zoom,
  setZoom,
}) => {
  const handlerZoomIn = () => setZoom(zoom + 1);
  const handlerZoomOut = () => setZoom(zoom - 1);

  return (
    <div className={styles.zoom_tool}>
      <a onClick={handlerZoomIn} className={styles.zoom_tool__item}>
        <svg
          width="24"
          height="24"
          viewBox="0 0 24 24"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            d="M12 5V19"
            stroke="#888888"
            strokeWidth="2"
            strokeLinecap="round"
            strokeLinejoin="round"
          />
          <path
            d="M5 12H19"
            stroke="#888888"
            strokeWidth="2"
            strokeLinecap="round"
            strokeLinejoin="round"
          />
        </svg>
      </a>
      <a onClick={handlerZoomOut} className={styles.zoom_tool__item}>
        <svg
          width="24"
          height="24"
          viewBox="0 0 24 24"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            d="M5 12H19"
            stroke="#888888"
            strokeWidth="2"
            strokeLinecap="round"
            strokeLinejoin="round"
          />
        </svg>
      </a>
    </div>
  );
};

export default LeafletZoomControl;
