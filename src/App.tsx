import { Route, Routes } from "react-router";
import { Footer } from "./components/Footer/Footer";
import { Header } from "./components/Header/Header";
import { HomePage } from "./pages/HomePage/HomePage";
import { ProjectsPage } from "./pages/ProjectsPage/ProjectsPage";
import { PublicationsPage } from "./pages/PublicationsPage/PublicationsPage";
import { ArticlesPage } from "./pages/ArticlesPage/ArticlesPage";
import { ContactsPage } from "./pages/ContactsPage/ContactsPage";
import { ProjectPage } from "./pages/ProjectPage/ProjectPage";
import { useState } from "react";
import { LocaleTypes } from "./locale/locale";
import { IntlProvider } from "react-intl";
import { RussianMessages } from "./locale/ru";
import { EnglishMessages } from "./locale/en";

function App() {
  const [language, setLanguage] = useState<string>(LocaleTypes.ru);
  document.querySelectorAll("video").forEach((v) => {
    v.setAttribute("pip", "false");
  });
  return (
    <IntlProvider
      locale={language}
      messages={language === LocaleTypes.ru ? RussianMessages : EnglishMessages}
    >
      <Header setLanguage={setLanguage} />
      <Routes>
        <Route path="/" element={<HomePage />} />
        <Route path="/projects" element={<ProjectsPage />} />
        <Route path="/projects/project/:id" element={<ProjectPage />} />
        <Route path="/public" element={<PublicationsPage />} />
        <Route path="/articles" element={<ArticlesPage />} />
        <Route path="/contacts" element={<ContactsPage />} />
      </Routes>
      <Footer />
    </IntlProvider>
  );
}

export default App;
