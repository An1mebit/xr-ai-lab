import { MediaType } from "../components/MediaLink/MediaLink";
import { IArticle } from "../models/articleModels/IArticle";
import Skolkovo from "../assets/skolkovo.png";
import { ArticleType } from "../components/ArticleFabric/ArticleFabric";

const publicationsCostants: IArticle[] = [
  {
    articleData: {
      ru: {
        authors: ["Кто-то", "Не знаю"],
        heading: "Распознавание чего-то",
        journal: "Вестник",
      },
      en: {
        authors: ["Somebody", "Dond know"],
        heading: "recognizing something",
        journal: "Vestnik",
      },
    },
    filePath: "1414",
    year: "2023",
    isVidio: false,
    mediaPath: "https://habr.com/ru/news/739686/",
    mediaType: MediaType.article,
    imagePath: Skolkovo,
    size: ArticleType.large,
  },
  {
    articleData: {
      ru: {
        authors: ["Кто-то", "Не знаю"],
        heading: "Распознавание чего-то",
        journal: "Вестник",
      },
      en: {
        authors: ["Somebody", "Dond know"],
        heading: "recognizing something",
        journal: "Vestnik",
      },
    },
    filePath: "1414",
    year: "2023",
    isVidio: false,
    mediaPath: "https://habr.com/ru/articles/739316/",
    mediaType: MediaType.article,
    size: ArticleType.medium,
  },
  {
    articleData: {
      ru: {
        authors: ["Кто-то", "Не знаю"],
        heading: "Распознавание чего-то",
        journal: "Вестник",
      },
      en: {
        authors: ["Somebody", "Dond know"],
        heading: "recognizing something",
        journal: "Vestnik",
      },
    },
    filePath: "1414",
    year: "2023",
    isVidio: false,
    mediaPath: "https://habr.com/ru/articles/701352/",
    mediaType: MediaType.article,
    size: ArticleType.small,
  },
  {
    articleData: {
      ru: {
        authors: ["Кто-то", "Не знаю"],
        heading: "Распознавание чего-то",
        journal: "Вестник",
      },
      en: {
        authors: ["Somebody", "Dond know"],
        heading: "recognizing something",
        journal: "Vestnik",
      },
    },
    filePath: "1414",
    year: "2023",
    isVidio: false,
    mediaPath: "https://habr.com/ru/articles/701352/",
    mediaType: MediaType.article,
    size: ArticleType.small,
  },
];

export default publicationsCostants;
