import { IProjectFeature } from "../models/projectModels/IProjectFeature";
import Skolkovo from "../assets/skolkovo.png";

const projectsFeatureConstants: IProjectFeature[] = [
  {
    id: "2",
    titleImage: Skolkovo,
    businessTaskImages: [
      Skolkovo,
      "https://sun9-13.userapi.com/_LXD070E0KCqiM47xj6AkBwRWbsXIylDl32Mtg/2LZEPFSaoGA.jpg",
    ],
    projectData: {
      ru: {
        title: "МАИ CV",
        subTitle:
          "Программно-аппаратный комплекс для детектирования дефектов на трубах",
        description:
          "В ходе реализации проекта, в сжатые сроки (один месяц) были проведены большие блоки работ, включающие в себя исследование и дальнейшую разработку. Созданный генератор синтетических данных может быть использован в смежных задачах, где необходима генерация датасетов для обучения нейронных сетей.",
        data: {
          date: "1 месяц",
          year: "2020",
          customer: "Центр БПЛА МАИ",
          command: [
            {
              role: "PM",
              FIO: "Олег Юсупов",
            },
            {
              role: "R&D",
              FIO: "Вадим Кондаратцев",
            },
            {
              role: "R&D",
              FIO: "Александр Крючков",
            },
            {
              role: "R&D",
              FIO: "Роман Чумак",
            },
            {
              role: "CG",
              FIO: "Роман Чумак",
            },
            {
              role: "DEV",
              FIO: "Андрей Иванов",
            },
          ],
        },
        businessTask:
          "Оптимизация времени и расходов на проведение сервисного обслуживания ТЭЦ, в которое входит осмотр труб на наличие поломок и дефектов, путем использования дронов (БПЛА) и программного обеспечения для анализа полученных материалов.Нашей задачей было реализовать прототип программы для демонстрации возможности детектирования дефектов на фотографиях и видео, полученных с БПЛА.Для этого мы разделили задачу на следующие этапы:— развертывание инструмента для разметки данных CVAT— создание инструмента для аугментации данных — создание генератора синтетических данных — обучение моделей нейронных сетей (YoloV4, DetectoRS) для анализа изображений — создание client-server приложения (на базе которого клиент может самостоятельно загружать полученный от беспилотника видеопоток и анализировать состояние труб на наличие дефектов)",
        explanations: [
          {
            title: "Research",
            description:
              "Оптимизация времени и расходов на проведение сервисного обслуживания ТЭЦ, в которое входит осмотр труб на наличие поломок и дефектов, путем использования дронов (БПЛА) и программного обеспечения для анализа полученных материалов.Нашей задачей было реализовать прототип программы для демонстрации возможности детектирования дефектов на фотографиях и видео, полученных с БПЛА.Для этого мы разделили задачу на следующие этапы:— развертывание инструмента для разметки данных CVAT— создание инструмента для аугментации данных— создание генератора синтетических данных— обучение моделей нейронных сетей (YoloV4, DetectoRS) для анализа изображений— создание client-server приложения (на базе которого клиент может самостоятельно загружать полученный от беспилотника видеопоток и анализировать состояние труб на наличие дефектов)",
          },
          {
            title: "Development",
            description:
              "В следующем этапе, на основе существующих систем обучения глубоких архитектур для задач компьютерного зрения, мы реализовали собственную систему для обучения выбранных архитектур, разметки данных, сбора логов и метрик, визуализации. Для решения задачи нами был создан собственный генератор синтетических данных, который позволил значительно сократить процесс поиска материалов для обучения нейросетей. Далее нами производилось обучение выбранных архитектур на различных датасетах (собранных совместно с партнерами и сгенерированных).Как результат мы разработали прототип web client-server приложение, в которое были внедрены нейросети, обученные на поиск визуальных дефектов на трубах. Клиент может самостоятельно загружать материалы, полученные от беспилотника, и получать анализ состояния труб, как визуальный, так и технический (json файл).",
          },
        ],
        links: [
          {
            name: "Известия",
            href: "",
          },
          {
            name: "Medium",
            href: "",
          },
          {
            name: "Хабр (часть 1)",
            href: "",
          },
          {
            name: "Хабр (часть 2)",
            href: "",
          },
          {
            name: "Behance",
            href: "",
          },
          {
            name: "Phygitalism",
            href: "",
          },
        ],
      },
      en: {
        title: "MAI CV",
        subTitle:
          "Hardware and software complex for detecting defects on pipes",
        description:
          "During the implementation of the project, large blocks of work were carried out in a short time (one month), including research and further development. The created synthetic data generator can be used in related tasks where generation of datasets is necessary for training neural networks.",
        data: {
          date: "1 month",
          year: "2020",
          customer: "MAI UAV Center",
          command: [
            {
              role: "PM",
              FIO: "Oleg Yusupov",
            },
            {
              role: "R&D",
              FIO: "Vadim Kondratiev",
            },
            {
              role: "R&D",
              FIO: "Alexander Kryuchkov",
            },
            {
              role: "R&D",
              FIO: "Roman Chumak",
            },
            {
              role: "CG",
              FIO: "Roman Chumak",
            },
            {
              role: "DEV",
              FIO: "Andrey Ivanov",
            },
          ],
        },
        businessTask:
          "Optimization of time and costs for the maintenance of the CHP, which includes inspection of pipes for breakdowns and defects, by using drones (UAVs) and software for analyzing the materials obtained.Our task was to implement a prototype program to demonstrate the possibility of detecting defects in photographs and videos obtained from UAVs.To do this, we have divided the task into the following stages:— deployment of a CVAT data markup tool — creation of a data augmentation tool — creation of a synthetic data generator — training neural network models (YoloV4, DetectoRS) for image analysis — creating a client-server application (based on which the client can independently download the video stream received from the drone and analyze the condition of pipes for defects)",
        explanations: [
          {
            title: "Research",
            description:
              "Optimization of time and costs for the maintenance of the CHP, which includes inspection of pipes for breakdowns and defects, by using drones (UAVs) and software for analyzing the materials obtained.Our task was to implement a prototype program to demonstrate the possibility of detecting defects in photographs and videos obtained from UAVs.To do this, we have divided the task into the following stages:— deployment of a CVAT data markup tool — creation of a data augmentation tool — creation of a synthetic data generator— training neural network models (YoloV4, DetectoRS) for image analysis — creating a client-server application (based on which the client can independently download the video stream received from the drone and analyze the condition of pipes for defects)",
          },
          {
            title: "Development",
            description:
              "In the next stage, based on the existing systems for training deep architectures for computer vision tasks, we implemented our own system for training selected architectures, marking data, collecting logs and metrics, visualization. To solve the problem, we created our own synthetic data generator, which significantly reduced the process of searching for materials for training neural networks. Next, we trained the selected architectures on various datasets (assembled together with partners and generated).As a result, we developed a prototype web client-server application in which neural networks trained to search for visual defects on pipes were implemented. The client can independently upload the materials received from the drone and receive an analysis of the condition of the pipes, both visual and technical (json file).",
          },
        ],
        links: [
          {
            name: "News",
            href: "",
          },
          {
            name: "Medium",
            href: "",
          },
          {
            name: "Habr (part 1)",
            href: "",
          },
          {
            name: "Habr (part 2)",
            href: "",
          },
          {
            name: "Behance",
            href: "",
          },
          {
            name: "Phygitalism",
            href: "",
          },
        ],
      },
    },
  },
];

export default projectsFeatureConstants;
