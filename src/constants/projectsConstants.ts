import { IProject, ProjectTypes } from "../models/projectModels/IProject";
import DefaultImage from "../assets/dora.png";

const projectsConstants: IProject[] = [
  {
    id: "1",
    type: ProjectTypes.link,
    projectData: {
      ru: {
        description:
          "Он был не только хорошим инженером, но и хорошим специалистом по вопросам экономики и организации производства, поэтому его избрали деканом факультета.",
        title: "Проект",
        subTitle: "От теории к практике, от исследований к проектам",
      },
      en: {
        description:
          "He was not only a good engineer, but also a good specialist in economics and the organization of production, so he was elected dean of the faculty.",
        title: "Project",
        subTitle: "From theory to practice, from research to projects",
      },
    },

    imagePath: DefaultImage,
    linkToProject: "https://olegteam.ru/",
  },
  {
    id: "2",
    type: ProjectTypes.current,
    projectData: {
      ru: {
        description:
          "Он был не только хорошим инженером, но и хорошим специалистом по вопросам экономики и организации производства, поэтому его избрали деканом факультета.",
        title: "Проект",
        subTitle: "От теории к практике, от исследований к проектам",
      },
      en: {
        description:
          "He was not only a good engineer, but also a good specialist in economics and the organization of production, so he was elected dean of the faculty.",
        title: "Project",
        subTitle: "From theory to practice, from research to projects",
      },
    },
    imagePath: DefaultImage,
  },
  {
    id: "2",
    type: ProjectTypes.current,
    projectData: {
      ru: {
        description:
          "Он был не только хорошим инженером, но и хорошим специалистом по вопросам экономики и организации производства, поэтому его избрали деканом факультета.",
        title: "Проект",
        subTitle: "От теории к практике, от исследований к проектам",
      },
      en: {
        description:
          "He was not only a good engineer, but also a good specialist in economics and the organization of production, so he was elected dean of the faculty.",
        title: "Project",
        subTitle: "From theory to practice, from research to projects",
      },
    },
    imagePath: DefaultImage,
  },
  {
    id: "2",
    type: ProjectTypes.current,
    projectData: {
      ru: {
        description:
          "Он был не только хорошим инженером, но и хорошим специалистом по вопросам экономики и организации производства, поэтому его избрали деканом факультета.",
        title: "Проект",
        subTitle: "От теории к практике, от исследований к проектам",
      },
      en: {
        description:
          "He was not only a good engineer, but also a good specialist in economics and the organization of production, so he was elected dean of the faculty.",
        title: "Project",
        subTitle: "From theory to practice, from research to projects",
      },
    },
    imagePath: DefaultImage,
  },
];

export default projectsConstants;
