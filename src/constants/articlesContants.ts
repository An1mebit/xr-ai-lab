import { MediaType } from "../components/MediaLink/MediaLink";
import { IArticle } from "../models/articleModels/IArticle";

const articleContants: IArticle[] = [
  {
    articleData: {
      ru: {
        authors: ["Кто-то", "Не знаю"],
        heading: "Распознавание чего-то",
        journal: "Вестник",
      },
      en: {
        authors: ["Somebody", "Dond know"],
        heading: "recognizing something",
        journal: "Vestnik",
      },
    },
    year: "2023",
    mediaType: MediaType.video,
    filePath: "",
    imagePath: "",
    mediaPath: "https://www.youtube.com/watch?v=JlqOFM5Q24w",
    isVidio: true,
  },
  {
    articleData: {
      ru: {
        authors: ["Кто-то", "Не знаю"],
        heading: "Распознавание чего-то",
        journal: "Вестник",
      },
      en: {
        authors: ["Somebody", "Dond know"],
        heading: "recognizing something",
        journal: "Vestnik",
      },
    },
    year: "2023",
    mediaType: MediaType.video,
    filePath: "",
    mediaPath: "https://www.youtube.com/watch?v=JlqOFM5Q24w",
    isVidio: true,
  },
  {
    articleData: {
      ru: {
        authors: ["Кто-то", "Не знаю"],
        heading: "Распознавание чего-то",
        journal: "Вестник",
      },
      en: {
        authors: ["Somebody", "Dond know"],
        heading: "recognizing something",
        journal: "Vestnik",
      },
    },
    year: "2023",
    mediaType: MediaType.video,
    filePath: "",
    mediaPath: "https://www.youtube.com/watch?v=JlqOFM5Q24w",
    isVidio: true,
  },
];

export default articleContants;
