import {
  AboutLab,
  Contacts,
  Partners,
  ProjectsCarousel,
  SearchProject,
} from "../../components/HomePageModules";

export const HomePage = () => {
  return (
    <>
      <SearchProject type={"home"} />
      <AboutLab />
      <Partners />
      <ProjectsCarousel />
      <Contacts />
    </>
  );
};
