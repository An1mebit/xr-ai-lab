import { SearchProject } from "../../components/HomePageModules";
import ProjectCard from "../../components/ProjectCard/ProjectCard";
import projectsConstants from "../../constants/projectsConstants";
import styles from "./projectspage.module.scss";

export const ProjectsPage = () => {
  return (
    <div className="projects">
      <SearchProject
        type="project"
        project={projectsConstants.filter((item) => item.type === "link")[0]}
      />
      <div
        className={styles.project__container}
        style={{
          display: "flex",
          gap: "20px",
          flexWrap: "wrap",
          justifyContent: "flex-start",
        }}
      >
        {projectsConstants
          .filter((item) => item.type !== "link")
          .map((project, index) => (
            <ProjectCard key={index} project={project} />
          ))}
      </div>
    </div>
  );
};
