import { FormattedMessage } from "react-intl";
import styles from "./contactsPage.module.scss";
import LeafletMap from "../../components/LeafletMap/LeafletMap";
import { useState } from "react";
import LeafletZoomControl from "../../components/LeafletMap/LeafletZoomControl/LeafletZoomControl";
import useWindowSize from "../../hooks/useWindowSize";
import { CarouselProvider, Slider, Slide } from "pure-react-carousel";
import "pure-react-carousel/dist/react-carousel.es.css";

export const ContactsPage = () => {
  const [zoom, setZoom] = useState<number>(15);
  const windowSize = useWindowSize();

  return (
    <div style={{ marginTop: "0px", paddingTop: "0", minHeight: "100vh" }}>
      <LeafletMap zoom={zoom} />
      <LeafletZoomControl zoom={zoom} setZoom={setZoom} />
      {windowSize >= 600 ? (
        <div className={styles.contacts__container}>
          <div className={styles.contacts__wrapper}>
            <div className={styles.contacts__item}>
              <h4>
                <FormattedMessage id={"contactsLocationMAI"} />{" "}
                <span>
                  {"("}
                  <FormattedMessage id="contactsLocationURU" />
                  {")"}
                </span>
              </h4>
              <h5>
                <FormattedMessage
                  id={"contactsLocationWhere"}
                  values={{ br: <br /> }}
                />
              </h5>
            </div>
            <div className={styles.contacts__item}>
              <h5>+7 (915) 234-17-26</h5>
              <p>
                <FormattedMessage id={"contactsWhereCall"} />
              </p>
            </div>
            <div className={styles.contacts__item}>
              <h5>itcentrmai@gmail.com</h5>
              <p>
                <FormattedMessage id={"contactsHow"} />
              </p>
            </div>
          </div>
        </div>
      ) : (
        <CarouselProvider
          naturalSlideWidth={100}
          naturalSlideHeight={125}
          totalSlides={2}
          infinite={true}
          visibleSlides={1.2}
          className={styles.contacts__wrapper_swipable}
        >
          <Slider style={{ height: "50vh" }}>
            <Slide key={0} index={0}>
              <div className={styles.contacts__item} style={{ width: "300px" }}>
                <h4>
                  <FormattedMessage id={"contactsLocationMAI"} />{" "}
                  <span>
                    {"("}
                    <FormattedMessage id="contactsLocationURU" />
                    {")"}
                  </span>
                </h4>
                <h5>
                  <FormattedMessage
                    id={"contactsLocationWhere"}
                    values={{ br: <br /> }}
                  />
                </h5>
              </div>
            </Slide>
            <Slide key={1} index={1}>
              <div className={styles.contacts__item}>
                <h5>+7 (915) 234-17-26</h5>
                <p>
                  <FormattedMessage id={"contactsWhereCall"} />
                </p>
                <h5>itcentrmai@gmail.com</h5>
                <p>
                  <FormattedMessage id={"contactsHow"} />
                </p>
              </div>
            </Slide>
          </Slider>
        </CarouselProvider>
      )}
    </div>
  );
};
