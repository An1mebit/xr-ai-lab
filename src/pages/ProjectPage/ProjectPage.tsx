import { useParams } from "react-router-dom";
import {
  ProjectBusinessTask,
  ProjectData,
  ProjectExplanation,
  ProjectFeature,
  ProjectLinks,
  ProjectTitle,
} from "../../components/ProjectPageModules";
import { IProjectFeature } from "../../models/projectModels/IProjectFeature";
import { useEffect, useState } from "react";
import projectsFeatureConstants from "../../constants/projectsFeatureConstants";
import { useIntl } from "react-intl";
import { LocaleTypes } from "../../locale/locale";
import { Link } from "react-router-dom";

export const ProjectPage = () => {
  const { id } = useParams();
  const [project, setProject] = useState<IProjectFeature>();
  const intl = useIntl();

  useEffect(() => {
    setProject(projectsFeatureConstants.find((item) => item.id === id));
  }, [id]);

  if (project !== undefined) {
    return (
      <>
        <Link
          id="aba"
          to={"/projects"}
          style={{
            position: "fixed",
            marginTop: "100px",
            marginLeft: "20px",
            zIndex: "6",
          }}
        >
          {"< "}Назад
        </Link>
        <ProjectTitle
          title={project.projectData[intl.locale as LocaleTypes].title}
          subTitle={project.projectData[intl.locale as LocaleTypes].subTitle}
          titleImage={project.titleImage}
        />
        <ProjectFeature
          description={
            project.projectData[intl.locale as LocaleTypes].description
          }
        />
        <ProjectData
          data={project.projectData[intl.locale as LocaleTypes].data}
        />
        <ProjectBusinessTask
          businessTask={
            project.projectData[intl.locale as LocaleTypes].businessTask
          }
          businessTaskImages={project?.businessTaskImages || []}
        />
        <ProjectExplanation
          explanations={
            project.projectData[intl.locale as LocaleTypes].explanations
          }
        />
        <ProjectLinks
          links={project.projectData[intl.locale as LocaleTypes].links}
        />
      </>
    );
  } else {
    return (
      <>
        <h1>Loading...</h1>
      </>
    );
  }
};
