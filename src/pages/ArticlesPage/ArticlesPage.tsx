import { FormattedMessage } from "react-intl";
import {
  ArticleFabric,
  ArticleType,
} from "../../components/ArticleFabric/ArticleFabric";
import articleContants from "../../constants/articlesContants";

export const ArticlesPage = () => {
  return (
    <div
      className="container"
      style={{ paddingBottom: "130px", marginTop: "0px", paddingTop: "160px" }}
    >
      <h2 style={{ paddingBottom: "60px" }}>
        <FormattedMessage id={"articleTitle"} />
      </h2>
      <div
        style={{
          display: "flex",
          gap: "20px",
          flexWrap: "wrap",
          justifyContent: "flex-start",
        }}
      >
        {articleContants.map((item, index) => (
          <ArticleFabric
            key={index}
            article={item}
            mediaType={item.mediaType}
            size={ArticleType.small}
          />
        ))}
      </div>
    </div>
  );
};
