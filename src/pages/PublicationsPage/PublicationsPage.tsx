import { ArticleFabric } from "../../components/ArticleFabric/ArticleFabric";
import { FormattedMessage } from "react-intl";
import publicationsCostants from "../../constants/publicationsConstants";

export const PublicationsPage = () => {
  return (
    <div
      className="container"
      style={{ paddingBottom: "130px", marginTop: "0px", paddingTop: "160px" }}
    >
      <h2 style={{ paddingBottom: "60px" }}>
        <FormattedMessage id={"publicationsTitle"} />
      </h2>
      <div
        style={{
          display: "flex",
          gap: "20px",
          flexWrap: "wrap",
          justifyContent: "flex-start",
        }}
      >
        {publicationsCostants.map((item, index) => (
          <ArticleFabric
            key={index}
            article={item}
            mediaType={item.mediaType}
            size={item.size}
          />
        ))}
      </div>
    </div>
  );
};
