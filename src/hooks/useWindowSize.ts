import { useCallback, useEffect, useState } from "react";

const useWindowSize = () => {
  const [windowSize, setWindowSize] = useState<number>(window.innerWidth);
  const handleWindowResize = useCallback(() => {
    setWindowSize(window.innerWidth);
  }, []);

  useEffect(() => {
    window.addEventListener("resize", handleWindowResize);

    return () => window.removeEventListener("resize", handleWindowResize);
  }, [handleWindowResize]);

  return windowSize;
};

export default useWindowSize;
