import { useCallback, useEffect, useState } from "react";

const useScroll = () => {
  const [windowSize, setWindowSize] = useState<number>(0);
  const handleWindowResize = useCallback(() => {
    setWindowSize(window.scrollY);
  }, []);

  useEffect(() => {
    window.addEventListener("scroll", handleWindowResize);

    return () => window.removeEventListener("scroll", handleWindowResize);
  }, [handleWindowResize]);

  return windowSize;
};

export default useScroll;
